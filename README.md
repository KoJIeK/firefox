# Mozilla Firefox: unplug WEB PUSH and serviceWorkers

## Отключить уведомления WEB PUSH и Service Workers в Mozilla Firefox

1. about:config

2. dom.push.enabled = False

3. dom.serviceWorkers.enabled = False

4. about:serviceworkers -> del all